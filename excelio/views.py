from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.core.files.storage import FileSystemStorage
import pandas as pd
from excelio.models import Users,Friends

def Home(request):
    if request.method == 'POST' and request.FILES['myfile']:
        print(request.POST.get('username'))
        user_exists=Users.objects.filter(username=request.POST.get('username')).exists()
        if user_exists:
            return render(request,'input.html', {'warning':'User already exists'})
        user = Users.objects.create(username=request.POST.get('username'))
        user.save()
        myfile = request.FILES['myfile']
        fs = FileSystemStorage(location='media')
        filename = fs.save(myfile.name,myfile)
        uploaded_file_url = fs.url(filename)
        data = pd.read_excel('media/%s'%filename)
        for index, row in data.iterrows():
            b = Friends.objects.create(user=user,name=row['Name'],phone=row['Phone'])
            b.save()
            print(index,row['Name'],row['Phone'])
        return render(request, 'input.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request,'input.html')


def Output(request):
    if request.method == 'POST':
        try:
            user = Users.objects.get(username=request.POST.get('username'))
        except:
            return render(request,'output.html',{'warning':'User Not found'})
        friends = Friends.objects.filter(user=user)
        return render(request,'output.html',{'data':friends})
    return render(request,'output.html')
