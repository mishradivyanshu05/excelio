from django.conf.urls import url
from django.urls import path
from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls import static


urlpatterns = [
    path('', views.Home, name='input'),
    path('output', views.Output, name='output'),
    url(r'^admin/', admin.site.urls),
]+static.static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

# if settings:
#     urlpatterns+=static.static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
#     urlpatterns+=static.static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
