from django.db import models

class Users(models.Model):
    username=models.CharField(max_length=50,null=False)
    def __str__(self):
        return self.name
class Friends(models.Model):
    user=models.ForeignKey(Users,on_delete=models.CASCADE)
    name=models.CharField(max_length=50)
    phone=models.CharField(max_length=10)
